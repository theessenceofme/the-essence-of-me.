Managing Emotional and Mental challenges with the subtle energy of colour. At the Essence of Me, I typically assist and support with the reconnective journey to the authentic self, through heart centered colour healing (HCCH) which promotes balance, wellbeing, inner peace, relaxation, clarity, focus and enables us to validate, acknowledge, love and nurture ourselves. RUN is an acronym for the strategy I use, which allows you to gently: Release, Unravel and Neutralise your emotional/mental pain, simply using the powerful and subtle vibration of Colour Energy! Eudes is a unique healer she works from the heart. The imagery injected into my mind was very powerful and worked on a quantum level balancing many aspects Tatiana.

Address: PO Box 946, Kemp House, 152-160 City Road, London, Greater London EC1V 2NX


Phone: +44 7752 230612
